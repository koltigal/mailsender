<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page trimDirectiveWhitespaces="true"%>
<%@page contentType="text/html; charset=UTF-8" %>
<html>
<meta charset="UTF-8">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">

    <!-- Font Awesome -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.14/css/mdb.min.css" rel="stylesheet">

    <!-- Bootstrap tooltips -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"
            type="text/javascript"></script>
    <!-- Bootstrap core JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"
            type="text/javascript"></script>
    <!-- MDB core JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.14/js/mdb.min.js"
            type="text/javascript"></script>
</head>
<body>

<div class="d-flex flex-column mb-4 col-5 text-center section mx-auto">
    <p class="h4 mb-2">Список прошлых рассылок</p>
    <table>
        <c:choose>
            <c:when test="${not empty birthdayDataList}">
                <c:forEach items="${birthdayDataList}" var="birthdayData">
                    <tr>
                        <td>
                            <div class="d-flex mb-2 border-top border-light">
                                <div class="col-10 text-left align-self-center">
                                    <c:out value="${birthdayData}"></c:out>
                                </div>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <tr>
                    <td><p>Вы пока не сделали ни одной рассылки</p></td>
                </tr>
            </c:otherwise>
        </c:choose>
    </table>
</div>

</body>
</html>