package com.getjavajob.training.mailSender.balchevskiyn.config;

import com.mongodb.MongoClient;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;

@Configuration
public class MongoConfig extends AbstractMongoConfiguration {

    @Override
    protected String getDatabaseName() {
        return "test";
    }

    @Override
    protected Collection<String> getMappingBasePackages() {
        List<String> packageList = new ArrayList<>();
        packageList.add("com.getjavajob.training.mailSender.balchevskiyn");
        return packageList;
    }

    @Override
    public MongoClient mongoClient() {
        return new MongoClient("localhost", 27017);
    }
}
