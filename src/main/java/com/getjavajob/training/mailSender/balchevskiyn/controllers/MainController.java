package com.getjavajob.training.mailSender.balchevskiyn.controllers;

import com.getjavajob.training.mailSender.balchevskiyn.dao.BirthdayDataDao;
import com.getjavajob.training.webproject1803.balchevskiyn.mailing.BirthdayData;
import javax.servlet.http.HttpServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/")
public class MainController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(MainController.class);

    @Autowired
    private BirthdayDataDao birthdayDataDao;

    @RequestMapping
    public ModelAndView showMailForm() {
        ModelAndView modelAndView = new ModelAndView("mailingList");
        modelAndView.addObject("birthdayDataList", birthdayDataDao.findAll(BirthdayData.class));
        return modelAndView;
    }
}
