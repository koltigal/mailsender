package com.getjavajob.training.mailSender.balchevskiyn.dao;

import com.getjavajob.training.webproject1803.balchevskiyn.mailing.BirthdayData;
import java.util.List;

public interface CustomBirthdayDataDao {

    public List<BirthdayData> findAll(Class entityClass);
}
