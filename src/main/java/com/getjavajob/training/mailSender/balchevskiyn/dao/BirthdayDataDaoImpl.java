package com.getjavajob.training.mailSender.balchevskiyn.dao;

import com.getjavajob.training.webproject1803.balchevskiyn.mailing.BirthdayData;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;

public class BirthdayDataDaoImpl implements CustomBirthdayDataDao {

    private final MongoOperations operations;

    @Autowired
    public BirthdayDataDaoImpl(MongoOperations operations) {
        this.operations = operations;
    }

    @Override
    public List<BirthdayData> findAll(Class entityClass) {
        return operations.findAll(BirthdayData.class);
    }
}
