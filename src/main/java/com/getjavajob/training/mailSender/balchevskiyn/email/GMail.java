package com.getjavajob.training.mailSender.balchevskiyn.email;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GMail {

    private static final Logger logger = LoggerFactory.getLogger(GMail.class);

    String fromEmail = "socialnetbalch@gmail.com";

    Properties emailProperties;

    public GMail() {
        emailProperties = System.getProperties();
        emailProperties.put("mail.smtp.port", "587");
        emailProperties.put("mail.smtp.auth", "true");
        emailProperties.put("mail.smtp.starttls.enable", "true");
        logger.info("GMail", "Mail server properties set.");
    }

    public void sendEmail(List<String> toEmailList, String emailBody) throws MessagingException, UnsupportedEncodingException {
        Session mailSession = Session.getDefaultInstance(emailProperties, null);
        MimeMessage emailMessage = new MimeMessage(mailSession);

        emailMessage.setFrom(new InternetAddress(fromEmail, fromEmail));
        for (String toEmail : toEmailList) {
            logger.info("toEmail: " + toEmail);
            emailMessage.addRecipient(Message.RecipientType.TO,
                    new InternetAddress(toEmail));
        }

        emailMessage.setSubject("Your friend's birthday in SocialNetBalch!");
        emailMessage.setText(emailBody);
        logger.info("Email Message created.");

        Transport transport = mailSession.getTransport("smtp");
        transport.connect("smtp.gmail.com", fromEmail, "fyn7jirf3cerf3");
        logger.info("allrecipients: " + emailMessage.getAllRecipients());
        transport.sendMessage(emailMessage, emailMessage.getAllRecipients());
        transport.close();
        logger.info("Email sent successfully.");
    }

}
