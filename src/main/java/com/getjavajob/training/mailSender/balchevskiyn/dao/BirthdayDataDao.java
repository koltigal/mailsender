package com.getjavajob.training.mailSender.balchevskiyn.dao;

import com.getjavajob.training.webproject1803.balchevskiyn.mailing.BirthdayData;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BirthdayDataDao extends MongoRepository<BirthdayData, String>, CustomBirthdayDataDao {

}
