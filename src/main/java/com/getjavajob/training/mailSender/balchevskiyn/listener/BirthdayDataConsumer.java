package com.getjavajob.training.mailSender.balchevskiyn.listener;

import com.getjavajob.training.mailSender.balchevskiyn.config.Application;
import com.getjavajob.training.mailSender.balchevskiyn.dao.BirthdayDataDao;
import com.getjavajob.training.mailSender.balchevskiyn.email.GMail;
import com.getjavajob.training.webproject1803.balchevskiyn.mailing.BirthdayData;
import java.io.UnsupportedEncodingException;
import javax.mail.MessagingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class BirthdayDataConsumer {
    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    @Autowired
    private BirthdayDataDao birthdayDataDao;

    @JmsListener(destination = "birthData.queue")
    public void consume(BirthdayData birthdayData) throws UnsupportedEncodingException, MessagingException {
        birthdayDataDao.save(birthdayData);
        GMail mail = new GMail();
        mail.sendEmail(birthdayData.accountFriendsEmails, "Hey, today is your friend's "
                + birthdayData.birthdayAccountName + " birthday, don't forget to congratulate in SocialNetBalch");
    }

}
