#MailSender

** Functionality: **

+ receive jms messages  
+ put message data to MongoDB  
+ send email to addresses from jms message  


** Tools: **  
JDK 8, SpringBoot 2, Spring Web MVC, JMS 2, ActiveMQ 5, Twitter Bootstrap 4, Maven 3, Git / Bitbucket, Tomcat 8, MongoDB, IntelliJIDEA 2018.  

--  
**����������� �������**  
������� getJavaJob,   
[http://www.getjavajob.com](http://www.getjavajob.com)
